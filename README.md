# 2023-Python-DONNÉ

## How to play
You should download the lexic at the url : https://elearning.univ-eiffel.fr/pluginfile.php/333004/mod_folder/content/0/Lexique383.tsv?forcedownload=1
And put it in the same folder of the script sutom-cli.py

You can run the program using the command : 
    python sutom-cli.py 

***Available options:***
  - (-h) Show the help menu.
  - (--debug FILE) Put debug logs into the specified file.
  - (--seed SEED) Define a seed which represent the word that need to be founded.
  - (--no-color) Disable text colorization
  - (--derived) Activate derived words
  - (--min) Set minimum word length
  - (--max) Set maximum word length
  - (--turn) Set maximum number of turn available to found the word

