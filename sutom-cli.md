% Sutom-cli
% INFO2 Python
% 2022 -- 2023

Le but de ce projet est de créer une version en ligne de commande du jeu
télévisé MOTUS.
Le site [SUTOM] donne une implémentation (graphique) de ce qu'on veut accomplir
(dans un terminal) avec ce projet.

[SUTOM]: https://sutom.nocle.fr/

Description
------------
Le programme choisit pour commencer un mot *cible* aléatoire et l'utilisateur
devra entrer un certain nombre de mots *tentative* afin de deviner la cible le
plus rapidement possible.

Au début, le programme donne la première lettre de la cible, ainsi que sa
longueur.  Par exemple si la cible est le mot `cibler`, alors il pourrait être
indiqué comme comme `c....`

L'utilisateur essaie ensuite de deviner le mot en entrant une première
tentative, par exemple `clair` et le programme indique pour chaque lettre si
elle est absente, mal placée, ou bien placée.

Par exemple, le programme pourrait afficher
```
    clamer
    c~  er
```
pour indiquer que le `l` est mal placé et que `e` et `r` sont bien placés.

L'utilisateur peut ensuite entrer une seconde tentative, puis une troisième,
etc.  Le jeu s'arrête quand l'utilisateur a entrée 6 fausses tentatives ou a
trouvé le mot cible.


Quelques détails à propos du projet minimal
--------------------------------------------
1. Le programme doit avoir une option `--debug=<nom de fichier>` qui permet
d'inscrire des informations supplémentaires dans le fichier donné en argument.
Cette option est faite pour que les testeurs puissent faire leur boulot
correctement, et que vous puissiez débuguer votre programme.

2. Le programme `sutom-cli.py` est un programme qui va choisir un mot *cible*
aléatoirement.  Pour pouvoir corriger un comportement incorrect du programme,
il faut pouvoir le reproduire et donc être capable de forcer le programme à se
comporter comme il s'est comporté dans le passé.  À cette fin, on ajoutera les
features suivants au programme:
   - une option `--seed=..` qui permet de choisir la *seed* aléatoire
   - si aucune *seed* n'est donnée sur la ligne de commande, choisissez là de
     manière appropriée
   - la *seed* utilisée est donnée dans le fichier de debug.

3. Le mot aléatoire sera tiré dans le lexique donné dans le fichier
   `Lexique383.tsv`.  La description officielle de ce fichier se trouve dans le
   fichier `Lexique3_manuel.pdf`

4. Les tentatives de l'utilisateur peuvent être incohérentes avec les
   informations qu'il a. Par exemple,
   - si la cible est `cibler`
   - et la première tentative est `clamer`
   - la deuxième tentative peut-être `claire` 
   alors qu'on sait que c'est impossible, car on sait que la deuxième lettre de
   la cible n'est pas un `l`.

   Par contre, toutes les tentatives devront être des mots de la bonne longueur
   et commencer par la même lettre que la cible.

5. Attention, la gestion du statut (absente/mal placée/bien placée) des lettres
   est moins facile qu'il n'y paraît, surtout quand il y a plusieurs fois la
   même lettre dans la cible et/ou la tentative.
   - Si une lettre est à la fois mal placée et bien placée (par ex., les `r`
     dans `cramer` si la cible est `cibler`), indiquez uniquement celle
     qui est bien placée (ici, le premier `r` de `cramer` est indiqué absent et
     le second est indiqué bien placé).
   - Si une lettre est plusieurs fois mal placée (par ex., les `r` dans
     `carrie` si la cible est `cibler`) seulement un des `r` doit être indiqué
     comme mal placé.
   - etc.


Améliorations
--------------
Il n'est pas attendu que vous implémentiez toutes les améliorations ci-dessous,
mais au moins quelques unes.  Vous pouvez bien-sûr implémenter des
améliorations qui ne sont pas indiquées ici, et les testeurs peuvent en
demander; dans ce cas, documentez les bien dans les options.

1. Ajouter une/des options qui permettent de fixer les longueurs minimales et
   maximales pour le mot cible. Des valeurs par défaut pour ces valeurs sont au
   choix.

2. Ajouter une option `--color/-c` (activée par défaut) qui fait en sorte que
   le programme indique les lettres absentes, bien-placées ou mal-placées par
   un code couleur.

3. Ajouter une option `--no-derived` (activée par défaut) qui fait en sorte que
   le mot cible ne soit jamais un mot *dérivé*.  Quand elle est absente, le mot
   cible ne pourra pas être un mot *dérivé*.  Par exemple, un verbe conjugué,
   un nom au pluriel, un adjectif au féminin sont des mots dérivé.  Les mots
   dérivés pourront toujours être des tentatives entrées par l'utilisateur.

4. Ajouter l'option `--most-common` qui prend un paramètre entier $n$, et ne
   tire le mot cible que parmi les $n$ mots les plus courants dans le corpus.

5. À des fins de debug, il pourra être utile d'ajouter une option `--target/-t`
   qui permet de choisir le mot cible.

6. Ajouter un mode "contre la montre" (avec `--timed-mode` et `--timeout=$n$`)
   dans lequel le but est de trouver le plus de mots en $n$ secondes.

7. Ajouter une interface graphique. On pourra l'activer ou la desactiver avec
   l'option `--gui` .

8. Rajouter un raccourci clavier qui affiche une suggestions de prochain mot à
   tenter pour l'utilisateur.  Cette suggestion devra utiliser toutes les
   informations disponibles à l'utilisateur.
