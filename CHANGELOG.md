### **v1.6** - 18-04-2023
- ***Added***
    - Added the - characters as default revealed characters.

### **v1.5** - 18-04-2023
- ***Added***
    - Added an option to select the maximum number of turn to found the word
    - Now display the word length and the rules of the game at the baginning of the game
    - Added code documentation
- ***Changed***
    - Code refactoring
    - Improved game interface
    - User can use uppercase but the result is transformed to lower case
    - User can't use digits in the word input
- ***Fixed***
    - Fixed a bug with the no color version of the game
    - Fixed a bug when user used the command CTRL+C
    
### **v1.4** - 18-04-2023
- ***Added***
    - Added command to restart or surrend the current game
- ***Changed***
    - Code refactoring
    - Improved documentation
    - Translation of english word to french

### **v1.3** - 04-04-2023
- ***Added***
    - Documentation on how to play
    - Added an option to select the minimum and maximum word length
- ***Changed***
    - Code refactoring
    - Now displaying the game board when user make a mistake
- ***Fixed***
    - Fixed a bug when displaying the game board

### **v1.2** - 04-04-2023
- ***Changed***
    - New game interface which also display history
- ***Fixed***
    - Fixed duplicated wrong character position on result computing
    - Fixed derived word disabled by default

### **v1.1** - 21-03-2023
- ***Added***
    - Added seed parameters to generate random index
    - Added text result colorization with option to disable it
    - Added option to disable derived words
- ***Changed***
    - Improved game interface
### **v1.0** - 21-03-2023
- ***Added***
    - Draw a ramdon word from the dictionary
    - Added debug output file to program args
    - Calculate user revealed, masked and wrong postion characters
    - Added main game loop