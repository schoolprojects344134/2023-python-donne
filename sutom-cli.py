import argparse
import sys
import random
import time

#region Classes definition

# Class which contains the game configuration (also control that the arguments are correct)
class GameConfig():
    debug: str
    seed: int | None
    noColor: bool
    derived: bool
    minWordLength: int
    maxWordLength: int
    maxTurn: int
    def __init__(self, debug: str, seed: int | None, noColor: bool, derived: bool, minWordLength: int, maxWordLength: int, maxTurn: int) :
        self.debug = debug
        self.seed = seed
        self.noColor = noColor
        self.derived = derived
        if maxWordLength < minWordLength:
            print("La longueur maximum d'un mot ne peut pas être inférieur à la longueur minimum !")
            exit(1)
        if minWordLength < 2:
            print("Un mot ne peut pas être de taille inférieur à 2 !")
            exit(1)
        self.minWordLength = minWordLength
        self.maxWordLength = maxWordLength
        if maxTurn < 1: 
            print("Le nombre de tour ne peut pas être inférieur à 1 !")
            exit(1)
        self.maxTurn = maxTurn

# Class definition to store the lexic
class Lexic:
    simpleWords: list = list()
    derivedWords: list = list()

    # Initialize the lexic
    def __init__(self, gameConfig: GameConfig) :
        try:
            f = open(WORD_DATA_FILE,"r",-1,"UTF-8")
        except:
            print("Impossible de trouver le lexic ! Assurer vous d'avoir téléchargé le fichier à l'url : https://elearning.univ-eiffel.fr/pluginfile.php/333004/mod_folder/content/0/Lexique383.tsv?forcedownload=1 et de l'avoir mis dans le même dossier que le script sutom-cli.py.")
            exit(1)
        
        lines = f.readlines()
        # Remove colomns titles
        lines.pop(0)
        # Add words to the list
        for i in range(0,len(lines)):
            splittedLine = lines[i].split("\t")
            self._add_word_in_list(self.derivedWords, splittedLine[0], gameConfig.minWordLength, gameConfig.maxWordLength)
            self._add_word_in_list(self.simpleWords, splittedLine[2], gameConfig.minWordLength, gameConfig.maxWordLength)
        f.close()
    
    # Draw a random word from the lexic
    def drawRandomWord(self, gameConfig: GameConfig):
        if gameConfig.seed == None:
            seed = random.randint(0,10000000)
        else: 
            seed = gameConfig.seed
        random.seed(seed)
        printLog("Seed used:" + str(seed), gameConfig)
        
        if gameConfig.derived:
            if len(self.derivedWords) > 0:
                randomIndex = random.randint(0,len(self.derivedWords))
                wordSelected = self.derivedWords[randomIndex]
                printLog("Random word draw from lexic : " + str(wordSelected), gameConfig)
                return wordSelected
        else:
            if len(self.simpleWords) > 0:
                randomIndex = random.randint(0,len(self.simpleWords))
                wordSelected = self.simpleWords[randomIndex]
                printLog("Random word draw from lexic : " + str(wordSelected), gameConfig)
                return wordSelected
        printLog(f"Fatal error, unable to found a word: Derived={str(gameConfig.derived)} Seed={str(seed)}", gameConfig)
        print("Aucun mot correspondant aux filtres seléctionnés n'a été trouvé ! Veuillez essayer avec d'autres valeurs de paramètre.")
        exit(1)

    # Check if the word is contained in the lexic
    def word_is_valid(self, derived: bool, word: str):
        if derived:
            return word in self.derivedWords
        else:
            return word in self.simpleWords

    # Private function used to add a word in list if it has a valid length
    def _add_word_in_list(self, list: list, word: str, minWordLength: int, maxWordLength: int):
        wordLength = len(word)
        if wordLength >= minWordLength and wordLength <= maxWordLength :
            list.append(word)

# Class definition for console colorization
class bcolors:
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    RED = '\033[91m'
    ORANGE = '\033[93m'
    CYAN = '\033[96m'
    ENDC = '\033[0m'
    HEADER = '\033[95m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
 
#Class definition for a turn
class Turn():
    turn: str
    def __init__(self, turn: str) :
        self.turn = turn

    def colorize(self):
        return f"{bcolors.BLUE}{self.turn}{bcolors.ENDC}"

# Class definition for a word 
class Word():
    computedWord: str
    userWord : str
    def __init__(self, computedWord: str, userWord: str) :
        self.computedWord = computedWord
        self.userWord = userWord

    def colorize(self):
        result = ""
        for i in range(0, len(self.computedWord)):
            if self.computedWord[i] == '.':
                result +=  f"{bcolors.RED}{self.userWord[i]}{bcolors.ENDC}"
            elif self.computedWord[i] == '~':
                result +=  f"{bcolors.ORANGE}{self.userWord[i]}{bcolors.ENDC}"
            else :
                result +=  f"{bcolors.GREEN}{self.userWord[i]}{bcolors.ENDC}"
        return result
    
# Class which represent a game turn 
class GameTurn():
    turn: Turn
    word: Word
    def __init__(self, turn: Turn, word: Word) :
        self.turn = turn
        self.word = word

#endregion

# Define program arguments
parser = argparse.ArgumentParser(
                    prog = "sutom-cli",
                    description = "Ce jeux est similaire au jeux télévisé 'Des chiffres et des lettres' et fonctionne entièrement dans la console de votre terminal. Bonne chance et amusé vous bien :). Pour obtenir plus d'information à propos du jeux rendez-vous sur le projet GitLab à l'url suivante : https://gitlab.com/marsault/2023-python-donne")

parser.add_argument("--debug", dest='debug',help="Chemin vers le fichier de destination pour les logs de debug.", type=str)
parser.add_argument("--seed", dest='seed',help="Définie manuellement le seed pour la génération du mot aléatoire (pratique pour le debug).", type=int)
parser.add_argument("--no-color", "-c", dest='noColor', action="store_true", help="Désactive la colorisation des lettres (Désactivé par défaut).", default=False)
parser.add_argument("--derived", dest='derived', action="store_true", help="Active les mots dérivées (Désactivé par défaut).", default=False)
parser.add_argument("--min", dest='min', help="Définie la longueur minimum des mots tirés aléatoirements (Utilise 4 par defaut).", default=4, type=int)
parser.add_argument("--max", dest='max', help="Définie la longueur maximum des mots tirés aléatoirements (Utilise 10 par defaut).", default=10, type=int)
parser.add_argument("--turn", dest='turnCount', help="Définie le nombre de tour maximum pour trouver le mot (Utilise 6 par defaut).", default=6, type=int)

sys.argv.pop(0) # Remove file path
# Parse arguments
args = parser.parse_args(sys.argv)

WORD_DATA_FILE = "Lexique383.tsv"

# Characters used to compute the user word
WRONG_CHARACTER_CHAR = "."
WRONG_CHARACTER_POSTION_CHAR = "~"

# Print logs in debug file
def printLog(message: str, gameConfig: GameConfig):
    '''
    Print log in the debug file
            Parameters:
                    message (str): The message to be logged
                    gameConfig (GameConfig): The game configuration
    '''
    logMessage = "DEBUG - " + time.strftime("%H:%M:%S +0000", time.gmtime()) + " - " + message
    if gameConfig.debug:
        debugFile = open(gameConfig.debug, "a", -1, "UTF-8")
        debugFile.write(logMessage + "\n")
        debugFile.close()

# Calculate result word using secret word and user input word
def processRules(secretWord: str,userWord: str):
    userWrongCharacters = []
    size = len(secretWord)
    resultWord = [None] * size # Initialize an empty list
    secretWordMaskedCharactersCount = dict()

    # Compute revealed and masked characters
    for i in range(0,size):
        if secretWord[i] == userWord[i]:
            resultWord[i] = secretWord[i]
        else:
            resultWord[i] = WRONG_CHARACTER_CHAR
            userWrongCharacters.append(userWord[i]) # Add user wrong characters to the list
            # Add masked character available 
            previousCharacterCount = secretWordMaskedCharactersCount.setdefault(secretWord[i],0)
            secretWordMaskedCharactersCount[secretWord[i]] = previousCharacterCount + 1

    # Remove maximum character avaimlable in result word
    for i in range(0,size):
        while userWrongCharacters.count(resultWord[i]) >  resultWord.count(resultWord[i]):
            userWrongCharacters.remove(resultWord[i])
    
    # Compute wrong characters position
    for i in range(0,size):
        if resultWord[i] == WRONG_CHARACTER_CHAR :
            for j in range(0 ,size):
                if resultWord[j] == WRONG_CHARACTER_CHAR or resultWord[j] == WRONG_CHARACTER_POSTION_CHAR:
                    if userWord[i] == secretWord[j] and userWrongCharacters.__contains__(userWord[i]):
                        availableCharacterCount = secretWordMaskedCharactersCount[userWord[i]]
                        if availableCharacterCount != None and availableCharacterCount > 0:
                            # Decrease available masked characters count
                            secretWordMaskedCharactersCount[userWord[i]] = availableCharacterCount - 1
                            # Remove character from wrog user characters position
                            userWrongCharacters.remove(userWord[i])
                            resultWord[i] = WRONG_CHARACTER_POSTION_CHAR
                            break
    return resultWord

# Return game result 
def process_turn(secretWord: str, userWord: str, currentTurn: str):
    resultList: list = processRules(secretWord,userWord)
    return GameTurn(Turn(currentTurn), Word("".join(resultList),userWord))

#region Game display functions
def create_line_border(model: list[int], separatorCharacter: str, lineCharacter: str):
    '''
    Create a line which repreent the top or the bottom of the table displayed
            Parameters:
                    model (list[int]): Contains the width of each column
                    separatorCharacter (str): Separator character between each column
                    lineCharacter (str): Used character to represent the line
            Returns:
                    (str): The formated line
    '''
    line = ""
    for columnIndex in range(0,len(model)):
        line += separatorCharacter
        for i in range(0,model[columnIndex]):
            line += lineCharacter
    line += separatorCharacter
    return line

def create_header_line(turnColumn: str, wordColumn: str, model: list[int], separatorCharacter: str):
    '''
    Create a line which represent the header of the table displayed
            Parameters:
                    turnColumn (str): The turn column title
                    wordColumn (str): The word column title
                    model (list[int]): Contains the width of each column
                    separatorCharacter (str): Separator character between each column
            Returns:
                    (str): The formated line
    '''
    content = ""
    content += separatorCharacter + " "  + turnColumn
    for i in range(0,model[0] - len(turnColumn) - 1):
        content += " "

    content += separatorCharacter + " "  + wordColumn
    for i in range(0,model[1] - len(wordColumn) - 1):
        content += " "

    content += separatorCharacter
    return content

def create_line_content(game: GameTurn, model: list[int], separatorCharacter: str, gameConfig: GameConfig):
    '''
    Create a line which represent the game turn result
            Parameters:
                    game (GameTurn): The game turn to be displayed
                    model (list[int]): Contains the width of each column
                    separatorCharacter (str): Separator character between each column
                    gameConfig (GameConfig): The game configuration
            Returns:
                    (str): The formated line
    '''
    content = ""
    previousTurnLength = len(game.turn.turn) + 1
    if gameConfig.noColor :
        content += separatorCharacter + " "  + game.turn.turn
    else:
        content += separatorCharacter + " "  + game.turn.colorize()
    for i in range(0,model[0] - previousTurnLength):
        content += " "

    previousWordLength =len(game.word.userWord) + 1
    if gameConfig.noColor :
        content += separatorCharacter + " "  + game.word.computedWord
    else:
        content += separatorCharacter + " "  + game.word.colorize()
    for i in range(0,model[1] - previousWordLength):
        content += " "
    content += separatorCharacter
    return content
#endregion

def display_game_board(board: list[GameTurn], gameConfig: GameConfig):
    '''
    Display the game board.
            Parameters:
                    board (list(GameTurn)): The list of all game turn
                    gameConfig (Lexic): The object which contains game configuration
    '''
    columnsSeparator = "+"
    columnsLine = "-"
    columnsColumn = "|"
    formatedText = ""
    # Used to format column size
    tableModel = [0] * 2

    turnColumn = "Tour"
    wordColumn = "Mot proposé"
    tableModel[0] = len(turnColumn) + 3
    tableModel[1] = len(wordColumn) + 3

    # Get columns width
    for game in board:
        # For each column compute text length
        turnTextLength = len(game.turn.turn) + 2
        if tableModel[0] < turnTextLength:
            tableModel[0] = turnTextLength

        wordTextLength = len(game.word.userWord) + 2
        if tableModel[1] < wordTextLength:
            tableModel[1] = wordTextLength

    # Create board
    formatedText += create_line_border(tableModel,columnsSeparator,columnsLine) + "\n"
    formatedText += create_header_line(turnColumn, wordColumn, tableModel, columnsColumn)  + "\n"
    for game in board:
        formatedText += create_line_border(tableModel,columnsSeparator,columnsLine) + "\n"
        formatedText += create_line_content(game, tableModel, columnsColumn, gameConfig) + "\n"
    formatedText += create_line_border(tableModel,columnsSeparator,columnsLine)
    print(formatedText)
    return tableModel[0] + 3


def compute_revealed_characters(secretWord: str, resultWord: str):
    '''
    Compute the result word with mandatory characters.
            Parameters:
                    secretWord (str): The secret word of the game
                    resultWord (str): The input word of the user
            Returns: 
                    (str): The result word computed
    '''
    resultWord[0] = secretWord[0]
    for i in range(1, len(secretWord)):
        if secretWord[i] == '-':
            resultWord[i] = secretWord[i]
    return resultWord

def check_revealed_characters(secretWord: str, resultWord: str):
    '''
    Check that the revealed characters are correctly set by the user
            Parameters:
                    secretWord (str): The secret word of the game
                    resultWord (str): The input word of the user
            Returns: 
                    (bool): True if the word is correct False otherwise
    '''
    if secretWord[0] != resultWord[0]:
        return False  
    for i in range(1, len(secretWord)):
        if secretWord[i] == '-' and resultWord[i] != secretWord[i]:
            return False
    return True

def game_play(lexic : Lexic, gameConfig: GameConfig):
    '''
    Start playing the game and manage the game loop
            Parameters:
                    lexic (Lexic): The lexic used to draw the secret word
                    gameConfig (Lexic): The object which contains game configuration
    '''
    while True:
        tryCount = 0
        gameHistory = []
        userInputTextOffset = 0
        
        # Draw the secret word
        secretWord = lexic.drawRandomWord(gameConfig)


        # Print user commands and rules  
        print("Vous devez trouver un mot qui est composé de " + str(len(secretWord)) + " lettres en " + str(gameConfig.maxTurn) + " tentatives maximum.")
        if gameConfig.noColor:
            print("Dans le mot résultant:\nSi la lettre est affiché alors elle est correcte et bien placée.\nSi la lettre est remplacée par un " + WRONG_CHARACTER_CHAR + " alors elle n'est pas présente dans le mot.\nSi la lettre est remplacée par un " + WRONG_CHARACTER_POSTION_CHAR + " alors la lettre est bien présente dans le mot mais elle est mal placée.")
        else:
            print("Dans le mot résultant:\nSi la lettre est affichée en vert alors elle est correcte et bien placée.\nSi la lettre est affichée en rouge alors elle n'est pas présente dans le mot.\nSi la lettre est affichée en orange alors la lettre est bien présente dans le mot mais elle est mal placée.")
        print("Pour abondonner entré la commande: -q\nPour rejouer entré la commande: -r")

        # Format the base word and create the initialisation game turn
        gameResultInitial = [WRONG_CHARACTER_CHAR] * len(secretWord)
        gameResultInitial = compute_revealed_characters(secretWord, gameResultInitial)
        defaultWordPreview = "".join(gameResultInitial)
        initialTurn = GameTurn(Turn(str(gameConfig.maxTurn)), Word(defaultWordPreview,defaultWordPreview))
        gameHistory.append(initialTurn)
        userInputTextOffset = display_game_board(gameHistory, gameConfig)

        # Main game loop
        while True:
            offset = ""
            # Define offset for user writting position
            for i in range(0,userInputTextOffset - 2):
                offset += " "
            userInput = input(offset + "> ")
            # Transform user response to lower case
            userInput = userInput.lower()
            printLog("User input: " + userInput, gameConfig)

            # Check the user word don't contains digit
            if any(char.isdigit() for char in userInput):
                print("Vous ne pouvez pas utiliser de chiffres dans le mot !")
                printLog("User used digit in the word", gameConfig)
                userInputTextOffset = display_game_board(gameHistory, gameConfig)
                continue
                

            # Check commands
            if userInput == "-q":    
                print("Au revoir ! Le mot secret était " + secretWord + " ;)")
                printLog("User surrended the game", gameConfig)
                return
            
            if userInput == "-r":
                print("La prochaine fois c'est la bonne ;) Le mot secret était " + secretWord + " :)")
                printLog("User restarted the game", gameConfig)
                break
            

            # Check the word length is correct
            if len(userInput) != len(secretWord):
                print("Votre mot n'a pas la bonne longueur.")
                userInputTextOffset = display_game_board(gameHistory, gameConfig)
                continue

            # Check that the first charactere is the same as the secret word 
            if not check_revealed_characters(secretWord, userInput):
                print("Votre mot doit contenir aux bonnes positions les caractères obligatoires tel que la première lettre du mot secret et les tirets.")
                printLog("User word d'ont start with the same letter as the secret word or the - in the secretWord", gameConfig)
                userInputTextOffset = display_game_board(gameHistory, gameConfig)
                continue
        
            # Check the word is contained in the lexic
            if not lexic.word_is_valid(gameConfig.derived, userInput):
                print("Votre mot n'a pas été trouvé dans notre lexic...")
                printLog("User enter a word which is not contained in the lexic", gameConfig)
                userInputTextOffset = display_game_board(gameHistory, gameConfig)
                continue
            
            # Compute the game turn
            tryCount+=1
            currentGame = process_turn(secretWord,userInput,str(gameConfig.maxTurn - tryCount))
            gameHistory.append(currentGame)
            userInputTextOffset = display_game_board(gameHistory, gameConfig)

            if userInput == secretWord:
                print("Bravo, vous avez gagné ! Le mot secret était " + secretWord + ".")
                printLog("User won the game", gameConfig)
                return

            if tryCount == gameConfig.maxTurn:
                print("Perdu ! Le mot secret était " + secretWord + " :)")
                printLog("User lose the game", gameConfig)
                return

# Create game configuration
gameConfig = GameConfig(args.debug, args.seed, args.noColor, args.derived, args.min, args.max, args.turnCount)

# Create the lexic
lexic = Lexic(gameConfig)

# Start game
try:
    game_play(lexic, gameConfig)
except KeyboardInterrupt: 
    print("Pressez de partir :) A bientôt !")
    printLog("User hard closed the game", gameConfig)
